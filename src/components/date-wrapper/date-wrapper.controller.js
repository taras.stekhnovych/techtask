export default function DateWrapperController() {
  this.flyOutChanged = (date) => {
    this.flyoutDate = date;
    if (this.flybackDate && (date.getTime() > this.flybackDate.getTime())) {
      this.flybackDate = new Date(this.flybackDate.setDate(this.flyoutDate.getDate() + 2));
      this.onFlyBackDateChange({ date: this.flybackDate });
    }
    this.onFlyOutDateChange({ date: this.flyoutDate });
  };

  this.flyBackChanged = (date) => {
    this.flybackDate = date;
    if (this.flyoutDate && (date.getTime() < this.flyoutDate.getTime())) {
      this.flyoutDate = new Date(this.flyoutDate.setDate(this.flybackDate.getDate() - 2));
      this.onFlyOutDateChange({ date: this.flyoutDate });
    }
    this.onFlyBackDateChange({ date: this.flybackDate });
  };
}
