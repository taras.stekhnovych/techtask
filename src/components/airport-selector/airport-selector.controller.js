export default class AirportSelectorController {
  constructor() {
    this.inputData = '';
    this.filterdAirports = [];
    this.isFocused = false;
    this.cancelBlur = false;
    this.wasHashRestored = false;
  }

  $onChanges(obj) {
    if (obj.airportsList) {
      this.filterdAirports = this.airportsList;
    }
    if (this.airportFromHash && !this.wasHashRestored) {
      this.inputData = this.airportFromHash;
      this.wasHashRestored = true;
    }
  }

  onInputValueChanged() {
    this.filterOptions();
    this.onValueChanged({ val: this.inputData });
  }

  filterOptions() {
    if (this.inputData) {
      this.filterdAirports = this.airportsList.filter((airport) => {
        return airport.name.toLowerCase().includes(this.inputData.toLowerCase());
      });
    } else {
      this.filterdAirports = this.airportsList;
    }
  }

  onAutocompleteSelect(event, airportName) {
    this.cancelBlur = true;
    this.inputData = airportName;
    this.filterdAirports = [{ name: airportName }];
    this.onValueChanged({ val: airportName });
  }

  onInputBlur(event) {
    if (this.cancelBlur) {
      event.preventDefault();
      this.cancelBlur = false;
      event.target.focus();
    } else {
      this.isFocused = false;
    }
  }
}
