import angular from 'angular';
import { DateWrapperComponent } from './date-wrapper/date-wrapper.component';
import { DateSelectorComponent } from './date-selector/date-selector.component';
import { AirportSelectorComponent } from './airport-selector/airport-selector.component';
import { CheapFlightsComponent } from './cheap-flights/cheap-flights.component';
import { FlightListComponent } from './flight-list/flight-list.component';

export default angular.module('app.components', [])
.component('dateWrapper', DateWrapperComponent)
.component('dateSelector', DateSelectorComponent)
.component('airportSelector', AirportSelectorComponent)
.component('cheapFlights', CheapFlightsComponent)
.component('flightList', FlightListComponent)
  .name;
