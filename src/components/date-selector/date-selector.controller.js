export default class DateSelectorController {
  $onChanges(data) {
    const date = data && data.date;
    if (date && date.currentValue) {
      this.date.setDate(date.currentValue.getDate());
    }
  }
}
