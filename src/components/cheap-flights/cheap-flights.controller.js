export default /* @ngInject */ class CheapFlightsController {
  constructor(AirportsService, CheapFlightService, $stateParams, $state, $window) {
    this.$state = $state;
    this.$window = $window;
    this.$stateParams = $stateParams;
    this.AiportsService = AirportsService;
    this.CheapFlightService = CheapFlightService;
    this.airports = [];
    this.departureAirport = '';
    this.destinationAirport = '';
    this.cheapestFlightsList = [];
    this.flyOutDate = '';
    this.flyBackDate = '';
    this.depAirportList = [];
    this.destAirportList = [];
  }

  $onInit() {
    this.AiportsService.getAirports().then((data) => {
      const dateParamsPresent = this.$stateParams.flyOut && this.$stateParams.flyBack;
      const airportParamsPresent = this.$stateParams.fromIata && this.$stateParams.fromIata;
      this.airports = data;
      this.destAirportList = this.depAirportList = data;

      if (dateParamsPresent) {
        this.flyOutDate = new Date(this.$stateParams.flyOut);
        this.flyBackDate = new Date(this.$stateParams.flyBack);
      }

      this.AiportsService.getRoutes().then(data => {
        this.routes = data;
        if (dateParamsPresent && airportParamsPresent) {
          this.setSelectedAirportsFromUrl(this.airports);
        }
      });
    });
  }

  getCheapestFlightsList() {
    this.setUrlParams();
    this.CheapFlightService.getCheapestFlight(this.getIataCode(this.departureAirport),
      this.getIataCode(this.destinationAirport), this.flyOutDate, this.flyBackDate).then((data) => {
        this.cheapestFlightsList = data.flights;
      });
  }

  setUrlParams() {
    this.$stateParams.fromIata = this.getIataCode(this.departureAirport);
    this.$stateParams.toIata = this.getIataCode(this.destinationAirport);
    this.$stateParams.flyOut = this.flyOutDate.toISOString().slice(0,10);
    this.$stateParams.flyBack = this.flyBackDate.toISOString().slice(0,10);
    this.$state.go(this.$state.$current, this.$stateParams, { notify: false });
  }

  findAirportByName(name) {
    return this.airports.find((airport) => airport.name === name);
  }

  setDepAirport(val) {
    const iataCode = this.getIataCode(val);
    this.departureAirport = val;
    this.$state.go(this.$state.$current, this.$stateParams, { notify: false });
    if (iataCode) {
      this.destAirportList = this.filterAirportsByIataCode(iataCode, this.airports);
    }
  }

  setDestAirport(val) {
    const iataCode = this.getIataCode(val);
    this.destinationAirport = val;
    this.$state.go(this.$state.$current, this.$stateParams, { notify: false });
    if (iataCode) {
      this.depAirportList = this.filterAirportsByIataCode(iataCode, this.airports);
    }
  }

  filterAirportsByIataCode(iataCode, airports) {
    const routesAr = this.routes[iataCode];
    if (!routesAr) return;
    return airports.filter((airport) => {
      return routesAr.includes(airport.iataCode);
    }) || [];
  }

  getIataCode(airport) {
    const airPort = this.findAirportByName(airport);
    return airPort ? airPort.iataCode : '';
  }

  setSelectedAirportsFromUrl(airports) {
    const depAirportIata = this.$stateParams.fromIata;
    const destAirportIata = this.$stateParams.toIata;
    this.$window.angular.forEach(airports, (airport) => {
      if (airport.iataCode === depAirportIata) {
        this.departureAirport = airport.name;
      } else if (airport.iataCode === destAirportIata) {
        this.destinationAirport = airport.name;
      }
    });
    this.destAirportList = this.filterAirportsByIataCode(destAirportIata, this.airports);
    this.getCheapestFlightsList();
  }

  setFlyOutDate(date) {
    this.flyOutDate = date;
  }

  setFlyBackDate(date) {
    this.flyBackDate = date;
  }
}
