import template from './cheap-flights.component.html';
import controller from './cheap-flights.controller';
import './cheap-flights.component.scss';

export const CheapFlightsComponent = {
  template,
  transclude: true,
  controller
};
