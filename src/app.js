import angular from 'angular';
import uiRouter from 'angular-ui-router';

import './scss/main.scss';

import Components from './components/components';
import { HomeComponent } from './home/home.component';
import {
  CheapFlightService,
  AirportsService
} from './services';

angular.module('myApp', [
  uiRouter,
  Components
])
.component('homePage', HomeComponent)
.service('AirportsService', AirportsService)
.service('CheapFlightService', CheapFlightService)
.config(($stateProvider, $urlRouterProvider) => {
  'ngInject';
  $stateProvider
    .state('home', {
      url: '/',
      template: '<home-page></home-page>'
    });
  $stateProvider.state('cheapestflight', {
    parent: 'home',
    url: 'cheapestflight/dep/:fromIata/desp/:toIata/flyout/:flyOut/flyback/:flyBack',
    template: '<cheap-flights></cheap-flights>',
    params: {
      fromIata: { dynamic: true, squash: true, value: '' },
      toIata: { dynamic: true, squash: true, value: '' },
      flyOut: { dynamic: true, squash: true, value: '' },
      flyBack: { dynamic: true, squash: true, value: '' },
    }
  });
  $urlRouterProvider.otherwise('/');
});
