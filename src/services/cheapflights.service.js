export /* @ngInject */ class CheapFlightService {
  //TODO: Write your own
  constructor($http) {
    'ngInject';
    this.$http = $http;
  }

  getCheapestFlight(startIata, destIata, startDate, destDate) {
    startDate = startDate.toISOString().slice(0,10);
    destDate = destDate.toISOString().slice(0,10);
    const apiPath = 'https://murmuring-ocean-10826.herokuapp.com/en/api';
    const url = `${apiPath}/2/flights/from/${startIata}/to/${destIata}/${startDate}/${destDate}/250/unique/?limit=15&offset-0`;
    return this.$http.get(url).then(data => data.data, error => error);
  }
}
