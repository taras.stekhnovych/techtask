export /* @ngInject */ class AirportsService {
  constructor($http, $q, $window) {
    'ngInject';
    this.airports;
    this.routes;
    this.$http = $http;
    this.$q = $q;
    this.$window = $window;
  }
  getAirports() {
    const defer = this.$q.defer();
    if (this.$window.angular.isUndefined(this.airports)) {
      this.fetchData().then((data) => {
        if (!data || !data.airports) {
          return defer.reject('no data');
        }
        this.routes = data.routes;
        this.airports = data.airports;
        return defer.resolve(data.airports);
      });
    } else {
      return this.$q.resolve(this.airports);
    }
    return defer.promise;
  }

  getRoutes() {
    const defer = this.$q.defer();
    if (this.$window.angular.isUndefined(this.routes)) {
      this.fetchData().then((data) => {
        if (!data || !data.routes) {
          return defer.reject('no data');
        }
        this.routes = data.routes;
        return defer.resolve(data.routes);
      });
    } else {
      return this.$q.resolve(this.routes);
    }
    return defer.promise;
  }

  fetchData() {
    const url = 'https://murmuring-ocean-10826.herokuapp.com/en/api/2/forms/flight-booking-selector/';
    return this.$http.get(url).then((response) => {
      return response.data;
    });
  }
}

